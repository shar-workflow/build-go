FROM golang:1.23.1
# To enable usage of docker on runner we still need the CLI, so we can install via this one liner:
# [ref https://stackoverflow.com/questions/38675925/is-it-possible-to-install-only-the-docker-cli-and-not-the-daemon]
COPY --from=docker:dind /usr/local/bin/docker /usr/local/bin/
RUN curl -sL https://deb.nodesource.com/setup_20.x | bash -
RUN apt-get update && apt-get install -y jq dpkg fakeroot unzip zip wget nodejs rpm && apt-get clean
RUN wget https://github.com/gohugoio/hugo/releases/download/v0.132.1/hugo_0.132.1_linux-amd64.deb && dpkg -i hugo_0.132.1_linux-amd64.deb && rm hugo_0.132.1_linux-amd64.deb && apt-get clean
RUN curl -LO https://github.com/protocolbuffers/protobuf/releases/download/v25.4/protoc-25.4-linux-x86_64.zip && unzip protoc-25.4-linux-x86_64.zip && rm protoc-25.4-linux-x86_64.zip
RUN go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
RUN go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.34.2
RUN go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.5.1
RUN go install github.com/vektra/mockery/v2@v2.44.1
